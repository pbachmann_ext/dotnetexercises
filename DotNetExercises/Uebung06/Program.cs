﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uebung06
{
    class Program
    {
        static void Main(string[] args)
        {

            List<Car> myCars = new List<Car>() {
            new Car{ PetName = "Henry", Color = "Silver", Speed = 100, Make = "BMW"},
            new Car{ PetName = "Daisy", Color = "Tan", Speed = 90, Make = "BMW"},
            new Car{ PetName = "Mary", Color = "Black", Speed = 55, Make = "VW"},
            new Car{ PetName = "Clunker", Color = "Rust", Speed = 5, Make = "Yugo"},
            new Car{ PetName = "Melvin", Color = "White", Speed = 43, Make = "Ford"}
            };


        }
    }
}
