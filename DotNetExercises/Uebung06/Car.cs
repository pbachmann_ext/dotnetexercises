﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uebung06
{
    public class Car
    {
        public string PetName { get; set; }
        public string Color { get; set; }
        public int Speed { get; set; }
        public string Make { get; set; }


        public void GetFastCars(List<Car> myCars)
        {
            var fastCars = from c in myCars where c.Speed > 55 select c;

            foreach (var car in fastCars)
            {
                Console.WriteLine("{0} schnelle Auto", car.PetName);
                Console.ReadLine();
            }
        }

        public void GetFastBMWs(List<Car> myCars)
        {

            var fastCars = from C in myCars where C.Speed > 90 && C.Make == "BMW" select C;
            foreach (var car in fastCars)
            {
                Console.WriteLine("{0} schnelle BMW", car.PetName);
                Console.ReadLine();
            }
        }
    }
}