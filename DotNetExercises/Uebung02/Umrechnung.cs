﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uebung02
{
    public static class Umrechnung
    {
        public static double CentimeterToInch(string result)
        {
            // Convert argument to double for calculations.
            double Centimeter = Double.Parse(result);
            // Convert Inch to Centimeter.
            double Inch = (Centimeter /2.54 );
            return Inch;
        }
        public static double InchToCentimeter(string result)
        {
            // Convert argument to double for calculations.
            double Inch = Double.Parse(result);
            // Convert Cenitmeter to Inch.
            double Centimeter = (Inch * 2.54);
            return Centimeter;
        }

    }
}