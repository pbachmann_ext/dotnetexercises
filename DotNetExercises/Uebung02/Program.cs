﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uebung02
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please select the convertor direction");
            Console.WriteLine("1. From Centimeter to Inch.");
            Console.WriteLine("2. From Inch to Centimeter.");
            Console.Write(":");

            string selection = Console.ReadLine();
            double C, I = 0;
            switch (selection)
            {
                case "1":
                    Console.Write("Please enter the Centimeter: ");
                    C = Umrechnung.CentimeterToInch(Console.ReadLine());
                    Console.WriteLine("Centimeter in Inch: {0:F2}", C);
                    break;
                case "2":
                    Console.Write("Please enter the Inch: ");
                    I = Umrechnung.InchToCentimeter(Console.ReadLine());
                    Console.WriteLine("Inch in Centimeter: {0:F2}", I);
                    break;
                default:
                    Console.WriteLine("Please select a convertor.");
                    break;
            }
            // Keep the console window open in debug mode.
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }
    }
}
