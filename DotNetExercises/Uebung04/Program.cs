﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uebung04
{
    class Program
    {
        private static Random rand = new Random();
        static void Main(string[] args)
        {
            Random r = new Random();
            int[] array = new int[100];
            for (int i = 0; i < array.Length; i++)
            {
                int nextInt = -1;
                while (Array.IndexOf(array, nextInt) != -1)
                {
                    nextInt = r.Next(1, 100);
                }
                array[i] = nextInt;
            }
            Console.WriteLine();
            Console.WriteLine("{0}", string.Join(", ", array));
            Console.ReadLine();
        }
    }
}