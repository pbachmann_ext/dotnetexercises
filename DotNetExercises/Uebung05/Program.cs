﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uebung05
{
    class Program
    {
        static void Main(string[] args)
        {
            //List
            List<String> strings = new List<String>();

            strings.Add("Hubert");
            strings.Add("Gunthart");
            strings.Add("Hubert");
            strings.Add("Akhil");

            Console.WriteLine();
            foreach (String aString in strings)
            {
                Console.WriteLine(aString);
                Console.ReadLine();
            }

            //Stack
            Stack<string> numbers = new Stack<string>();
            numbers.Push("one");
            numbers.Push("two");
            numbers.Push("three");
            numbers.Push("four");
            numbers.Push("five");

            // A stack can be enumerated without disturbing its contents.
            foreach (string number in numbers)
            {
                Console.WriteLine(number);
                Console.ReadLine();
            }
            //Queue
            Queue<string> zahlen = new Queue<string>();
            zahlen.Enqueue("one");
            zahlen.Enqueue("two");
            zahlen.Enqueue("three");
            zahlen.Enqueue("four");
            zahlen.Enqueue("five");

            // A queue can be enumerated without disturbing its contents.
            foreach (string zahl in zahlen)
            {
                Console.WriteLine(zahl);
                Console.ReadLine();
            }
        }               
    }
}
