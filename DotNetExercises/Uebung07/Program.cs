﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Uebung07
{
    class Program
    {
        static void Main(string[] args)
        {
            DirectoryInfo di = new DirectoryInfo(@"O:\EDV\Jonas.Zehnder\Temp\");
            var query = from fileinfo in di.GetFiles()
                        orderby fileinfo.LastAccessTime
                        select fileinfo;
            foreach (var fi in query)
            {
                Console.WriteLine(fi.FullName);
                Console.ReadLine();
            }           
        }
    }
}
